#
# A more complicated makefile
#

DEVKITARM := /home/nick/Downloads/devkitARM
TONCLIB   := /home/nick/Code/code/tonclib

PATH := $(DEVKITARM)/bin:$(PATH)

# --- Project details -------------------------------------------------

PROJ    := first
TARGET  := $(PROJ)

OBJP    := df.o path.o
OBJS    :=  Phoebus_16x16.o Jdpage_8x8.o $(TONCLIB)/src/tonc_oam.o $(TONCLIB)/src/tonc_input.o $(TONCLIB)/src/tonc_core.o 
SOBJ    := /home/nick/Code/code/tonclib/asm/tonc_memcpy.o

# --- Build defines ---------------------------------------------------

PREFIX  := $(DEVKITARM)/bin/arm-none-eabi-
CPP     := $(PREFIX)g++
CC      := $(PREFIX)gcc
LD      := $(PREFIX)g++
OBJCOPY := $(PREFIX)objcopy
AS      := $(PREFIX)as

ARCH    := -mthumb-interwork -mthumb
SPECS   := -specs=gba.specs

CFLAGS  := $(ARCH) -O5 -Wall -fno-strict-aliasing -I$(TONCLIB)/include
LDFLAGS := $(ARCH) $(SPECS)
ASFLAGS  := $(ARCH) -I$(TONCLIB)/include

.PHONY : build clean

# --- Build -----------------------------------------------------------
# Build process starts here!
all: $(TARGET).gba
	vba -4 $(TARGET).gba 

# Strip and fix header (step 3,4)
$(TARGET).gba : $(TARGET).elf
	$(OBJCOPY) -v -O binary $< $@
	-@gbafix $@

# Link (step 2)
$(TARGET).elf : $(OBJS) $(SOBJ) $(OBJP)
	$(LD) $^ $(LDFLAGS) -o $@

# Compile (step 1)
$(OBJP) : %.o : %.cpp
	$(CPP) -c $< $(CFLAGS) -o $@

$(OBJS) : %.o : %.c
	$(CC) -c $< $(CFLAGS) -o $@

$(SOBJ) : %.o : %.s
	#$(AS) -c $< $(AFLAGS) -o $@
	$(CC) -MMD -MP -MF $(DEPSDIR)/$*.d -x assembler-with-cpp $(ASFLAGS) -c $< -o $@
	
		
# --- Clean -----------------------------------------------------------

clean : 
	@rm -fv *.gba
	@rm -fv *.elf
	@rm -fv $(OBJP) $(OBJS) $(SOBJ)

#EOF