
#include "tonc.h"
#include "tonc_video.h"
#include "tonc_input.h"

#include "Phoebus_16x16.h"
#include "Jdpage_8x8.h"

#include "path.hpp"
#include "queue.hpp"

//#include <stdlib.h>
#include <string.h>

void VBlankIntrWait()
{   swi_call(0x05); }

OBJ_ATTR obj_buffer[128];

#define CBB_0  0
#define SBB_0 28

SCR_ENTRY *bg0_map= se_mem[SBB_0];

#define CHAR_0 4
#define WALL0 (11*16)
#define MARKED '#'
const char GROUND[] = { ',','.', ':', ';', '`', '\''};
/*#define GROUND0 ','//(11*16+2)
#define GROUND1 '.'//(11*16+2)
#define GROUND2 ':'//(11*16+2)
#define GROUND3 ';'//(11*16+2)
#define GROUND4 '`'//(11*16+2)
#define GROUND5 '\''//(11*16+2)
*/
#define STAIRS (60)
#define EMPTY  0
//#define WATER (7*16+15) // '~'
#define PLANT  3
#define WATER '~'


#define SIGN(a) (a<0?-1:(a>0?+1:0))

char getGroundTile(){
	int i = qran();
	return GROUND[ i % 6];
}

bool isGroundTile( char tile){
	for( int i=0; i<6; i++)
		if( tile == GROUND[i])
			return true;
	return false;
}

Path* getPath(Position* ori, Position* dest, bool verbose, bool, char);

void setBackgroundTile( char tile, int x, int y, int tw, int th){
	//unsigned char c = 40;
	//const unsigned short tile = c;
	//bg0_map[x*tw +0 + y*th*32*tw]  = SE_PALBANK(0) | (tile*tw*th);
	int offset = x*tw + y*th*32;
	for(int i=0; i<tw; i++)
		for(int j=0; j<th; j++)
			if(tile == WATER)
				bg0_map[ offset + i + 32*j] = SE_PALBANK(0) | (tile*tw*th + i+j*tw);
			else if(tile == PLANT)
				bg0_map[ offset + i + 32*j] = SE_PALBANK(2) | (tile*tw*th + i+j*tw);
			else
				bg0_map[ offset + i + 32*j] = SE_PALBANK(1) | (tile*tw*th + i+j*tw);

	/*
	bg0_map[x*tw+0 + y*th*32]  = SE_PALBANK(0) | (tile*tw*th);
	bg0_map[x*tw+1 + y*th*32]  = SE_PALBANK(0) | (tile*tw*th+1);
    bg0_map[x*tw+0 + y*th*32+32] = SE_PALBANK(0) | (tile*tw*th+2);
    bg0_map[x*tw+1 + y*th*32+32] = SE_PALBANK(0) | (tile*tw*th+3);
	*/
}

void putText( const char* text, int x, int y){
	for( int i=0; i<strlen(text); i++){
		setBackgroundTile(text[i], x+i, y, 1,1);
	}
}

#define nlines 16
#define ncols  16
#define ndepth 8

char map[ndepth][nlines][ncols];/* = {
		{1,1,1,1,1},
		{1,1,1,1,1},
		{1,1,1,1,1}};*/
//int ***dist;
//int dist[ndepth][nlines][ncols];
//Position prev[ndepth][nlines][ncols];
//Position ***prev;
dist_t dist[ndepth][nlines][ncols];
Position prev[ndepth][nlines][ncols];







class Dwarf;

enum {DIG, BUILD, B_STAIRS, Drink, Eat} JobType;
const char* JobNames[] = {
		"Digging", "Building", "Build Stairs", "Drinking", "Eating"
};
int numJobTypes = 3;
int jobType = DIG;

class Job{
public:
	Position p;
	int jobtype;
	Dwarf *dwarf = 0;
	Job( Position pos, int type){
		p = pos;
		jobtype = type;
	}
	Job( pos_t x, pos_t y, pos_t z, int type){
		p = {x,y,z};
		jobtype = type;
	};
	Position pos(){
		return p;
	}
	int type(){
		return jobtype;
	}
};

/*
class JobList{
	Job *jobList[10];
	int jobCount = 0;
public:
	JobList(){};
	int size(){return jobCount;}
	Job *get(int index){ return jobList[index];}
	void add(Job *job){
		jobList[jobCount] = job;
		jobCount++;
	}
	void remove(Job *job){
		for(int i=0; i<jobCount; i++){
			if( job == jobList[i]){
				jobCount--;
				jobList[i] = jobList[jobCount];
				return;
			}
		}
	}
};
*/


//Job *jobList[10];
//int jobCount = 0;
List<Job, 10> jobList;

//void assignJob( Job* job, Dwarf* dwarf, Path* path);


class Dwarf{
private:
	Position p;
	OBJ_ATTR *sprite;
	u32 tid=4, pb=0;      // (3) tile id, pal-bank
	int counter = 0;
public:
	float waterSupply;
	float foodSupply;
	Job *job = 0;
	Path *path = 0;
	int pathPos = 0;
	Dwarf(int objID,  Position pos){
		sprite = &obj_buffer[objID];
		p = pos;
		replenishFood();
		replenishWater();
	};
	Dwarf(int objID,  pos_t x,  pos_t y, pos_t z){
		sprite = &obj_buffer[objID];
		p = {x,y,z};
		replenishFood();
		replenishWater();
	};
	void replenishFood(){
		foodSupply = 1.;
	}
	void replenishWater(){
		waterSupply = 1.;
	}
	void assignJob( Job* job, Path* path)
	{
		jobList.add( job);

		job->dwarf = this;

		this->job = job;
		this->path = path;
		this->pathPos = this->path->size()-1;

	}

	void update(int visible_z){
		// job?
		if( job==0) // no job
			tid = 1*1;
		else{
			tid = 1*2;
			// move
			//x += SIGN( job->x-x);
			//y += SIGN( job->y-y);
		}

		// consumption
		if( job){
			waterSupply-= 0.0002;
			foodSupply -= 0.0002;

		}else{
			waterSupply-= 0.0001;
			foodSupply -= 0.0001;
		}

		// thirst level
		if( waterSupply < 0.1 and job==0){
			Path* path = getPath( &p, 0, false, false, WATER);
			if( path){
				Job* job = new Job( path->get(0), Drink);
				assignJob( job, path);
			}
		}
		if( foodSupply < 0.1 and job==0){
			Path* path = getPath( &p, 0, false, false, PLANT);
			if( path){
				Job* job = new Job( path->get(0), Eat);
				assignJob( job, path);
			}
		}

		if(counter % 20 == 0)
			if( path){
				if(pathPos >= 0){
					// reach destination
					p = path->get(pathPos);
					//map[x][y] = 'i';
					pathPos--;
				}else{
					switch( job->type()){
					case DIG:
						// dig
						map[job->pos().z][job->pos().x][job->pos().y] = getGroundTile();
						break;
					case BUILD:
						map[job->pos().z][job->pos().x][job->pos().y] = WALL0;
						break;
					case B_STAIRS:
						map[job->pos().z][job->pos().x][job->pos().y] = STAIRS;
						break;
					case Drink:
						replenishWater();
						break;
					case Eat:
						replenishFood();
						break;

					}
					jobList.remove(job);
					delete job;
					job = 0;
					delete path;
					path = 0;
				}
			}
		counter ++;

		if( p.z == visible_z){
			// paint
			obj_set_attr(sprite,
						ATTR0_SQUARE,              // Square, regular sprite
						ATTR1_SIZE_8x8,              // 64x64p,
						//ATTR1_SIZE_16x16,              // 64x64p,
						ATTR2_PALBANK(pb) | tid);
			//obj_set_pos(sprite, x*16, y*16);
			obj_set_pos(sprite, p.x*8, p.y*8);
		}else{
			obj_set_attr(sprite,
						ATTR0_SQUARE,              // Square, regular sprite
						ATTR1_SIZE_8x8,              // 64x64p,
						//ATTR1_SIZE_16x16,              // 64x64p,
						ATTR2_PALBANK(pb) | 0);
		}
	}
	Position pos(){
		return p;
	}
};

class Cursor{
private:
	int x, y;
	u32 tid, pb=0;      // (3) tile id, pal-bank
	OBJ_ATTR *cursor;
	int blink=0;
public:
	Cursor(int objID){
		cursor = &obj_buffer[objID];
		x = 10;
		y = 10;

	}
	void update(int visible_z){
		/*
		if( key_is_down( KEY_A) and !key_was_down( KEY_A)){
			//setBackgroundTile( 0, x/8, y/8);
			if( map[visible_z][x][y] == WALL0){
				if( jobList.add( new Job(x,y,visible_z, DIG)))
					map[visible_z][x][y] = MARKED;
			}
		}
		if( key_is_down( KEY_B) and !key_was_down( KEY_B)){
			if( map[visible_z][x][y] == EMPTY){
				if( jobList.add( new Job(x,y,visible_z, BUILD)))
					map[visible_z][x][y] = MARKED;
			}
		}*/
		if( key_is_down( KEY_A) and !key_was_down( KEY_A)){
			// rotate job type
			jobType = (jobType+1)%numJobTypes;
		}
		if( key_is_down( KEY_B) and !key_was_down( KEY_B)){
			// trigger job
			switch( jobType){
			case DIG:
				if( map[visible_z][x][y] == WALL0){
					if( jobList.add( new Job(x,y,visible_z, DIG)))
						map[visible_z][x][y] = MARKED;
				}
				break;

			case B_STAIRS:
				if( isGroundTile( map[visible_z][x][y]) or map[visible_z][x][y] == WALL0){
					if( jobList.add( new Job(x,y,visible_z, B_STAIRS)))
						map[visible_z][x][y] = MARKED;
				}
				break;


			}
		}

		blink = (blink + 1) % 40;
		if( blink < 10)
			tid = 1*'|';
		else if(blink < 20)
			tid = 1*'/';
		else if(blink < 30)
			tid = 1*'-';
		else if(blink < 40)
			tid = 1*'\\';
		obj_set_attr(cursor,
				ATTR0_SQUARE,              // Square, regular sprite
				ATTR1_SIZE_8x8,              // 64x64p,
				//ATTR1_SIZE_16x16,              // 64x64p,
				ATTR2_PALBANK(pb) | tid);

		if( !key_was_down(KEY_LEFT) and !key_was_down(KEY_RIGHT))
			x += key_tri_horz();
		if( !key_was_down(KEY_UP) and !key_was_down(KEY_DOWN))
			y += key_tri_vert();

		//obj_set_pos(cursor, (x/8)*16, (y/8)*16);
		obj_set_pos(cursor, x*8, y*8);
	}
};


// PATH FINDING

typedef PriorityQueue<Position, 100> PositionQueue;
PositionQueue queue;
Position neighborHood[6] = {
		{-1, 0, 0},
		{+1, 0, 0},
		{ 0,-1, 0},
		{ 0,+1, 0},
		{ 0, 0,-1},
		{ 0, 0,+1}
};
enum {Horizontal, Up, Down} MovementType;
int directions[6] = {
		Horizontal,
		Horizontal,
		Horizontal,
		Horizontal,
		Down,
		Up
};


bool reachable( int direction, char origTile, char destTile){
	if( ( direction == Horizontal and ( isGroundTile( origTile) or origTile == STAIRS)) or
		(direction == Down and origTile == STAIRS) or
		(direction == Up   and origTile == STAIRS))
		return true;
	else
		return false;
}
bool movable( int direction, char origTile, char destTile){
	if( ( direction == Horizontal and (isGroundTile(origTile) or origTile == STAIRS) and (isGroundTile(destTile) or destTile == STAIRS)) or
		(direction == Down and origTile == STAIRS and destTile == STAIRS) or
		(direction == Up   and origTile == STAIRS and destTile == STAIRS))
		return true;
	else
		return false;
}

Path* getPath(Position* ori, Position* dest, bool verbose, bool sameDepth, char destTile){



    for(int d=0; d<ndepth; d++)
        for(int l=0; l<nlines; l++)
        	for(int c=0; c<ncols; c++){
				dist[d][l][c] = 1000;
				prev[d][l][c] = {0,0,0};
			}

    queue.put( 0, *ori);
    dist[ ori->z][ ori->x][ori->y] = 0;
    prev[ ori->z][ ori->x][ori->y] = *ori;

    // neighbor transitions
    //Position neighborHood[4] = {{-1,0}, {+1,0}, {0,-1},{0,+1}};

    //int steps = 0;

    while( queue.size() > 0){
    	if( verbose)
    		printf("QUEUE LENGTH: %d\n", queue.size());
    	PositionQueue::QueueElement entry = queue.get();
    	Position pos = entry.data;
    	//int cost = entry.priority;
    	//printf("current node: %d, %d, %d -> cost: %d\n", pos.x, pos.y, pos.z, cost);

    	for( int i=0; i<6; i++){
    		Position neighbor;
    		neighbor = pos + neighborHood[i];
    		if( neighbor.x >= 0 and neighbor.y>=0 and neighbor.z>=0 and neighbor.x < nlines and neighbor.y < ncols and neighbor.z < ndepth){
    			if( verbose)
    			    printf("neighbor node: %d, %d, %d\n", neighbor.x, neighbor.y, neighbor.z);
				//printf("proceed!\n");

    			bool r = reachable(directions[i], map[ pos.z][ pos.x][ pos.y], map[neighbor.z][neighbor.x][ neighbor.y]);
    			bool m = movable(directions[i], map[ pos.z][ pos.x][ pos.y], map[neighbor.z][neighbor.x][ neighbor.y]);
    			if( true ){
					if( ((dest!=0 and neighbor == *dest) or (dest==0 and map[neighbor.z][neighbor.x][ neighbor.y] == destTile)) and r and (!sameDepth or pos.z==neighbor.z) ){
						//printf( "found!\n");

						Path *path = new Path( dist[ pos.z][ pos.x][ pos.y]);
						//printf("build path of length: %d\n", dist[ pos.z][ pos.x][ pos.y]);
						//if( include_dest):
						//	path.append( dest)

						Position *current = &pos;
						while( !(*current == *ori)){
							path->add( *current);
							current = &prev[ current->z][ current->x][ current->y];
							//#print current
						}
						queue.clear();
						return path;

						//return 0;
					}else{
						if( m){
							dist_t new_cost = dist[ pos.z][ pos.x][ pos.y] + 1;
							if( dist[neighbor.z][neighbor.x][ neighbor.y] > new_cost){
								dist[neighbor.z][neighbor.x][ neighbor.y] = new_cost;
								prev[neighbor.z][neighbor.x][ neighbor.y] = pos;
								if( verbose)
									printf("put neighbor in queue\n");
								queue.put( dist[neighbor.z][neighbor.x][ neighbor.y], neighbor);
							}
						}
					}
    			}
    		}


    	}

    }

    if( verbose)
    	printf( "Tile not reachable!\n");
    return 0;
}

void init_map()
{
    //int ii, jj;

    // initialize a background
    REG_BG0CNT= BG_CBB(CBB_0) | BG_SBB(SBB_0) | BG_REG_64x64;
    REG_BG0HOFS= 0;
    REG_BG0VOFS= 0;

    // (1) create the tiles: basic tile and a cross
/*    const TILE tiles[2]=
    {
        {{0x11111111, 0x01111111, 0x01111111, 0x01111111,
          0x01111111, 0x01111111, 0x01111111, 0x00000001}},
        {{0x00000000, 0x00100100, 0x01100110, 0x00011000,
          0x00011000, 0x01100110, 0x00100100, 0x00000000}},
    };
    tile_mem[CBB_0][0]= tiles[0];
    tile_mem[CBB_0][1]= tiles[1];
*/
    //memcpy32(&tile_mem[CBB_0][0], Phoebus_16x16Tiles, Phoebus_16x16TilesLen/4);
    memcpy32(&tile_mem[CBB_0][0], Jdpage_8x8Tiles, Jdpage_8x8TilesLen/4);

    // (2) create a palette
/*    pal_bg_bank[0][1]= RGB15(31,  0,  0);
    pal_bg_bank[1][1]= RGB15( 0, 31,  0);
    pal_bg_bank[2][1]= RGB15( 0,  0, 31);
    pal_bg_bank[3][1]= RGB15(16, 16, 16);
*/
    //memcpy32(pal_bg_mem, Phoebus_16x16Pal,   Phoebus_16x16PalLen);
    //memcpy32(pal_bg_mem, Jdpage_8x8Pal,   Jdpage_8x8PalLen);

    // LBLUE
    pal_bg_bank[0][2]= RGB15(0,  0,  15);
    pal_bg_bank[0][2]= RGB15(0,  0,  31);

    // LGREEN
	pal_bg_bank[2][1]= RGB15(0,15,  0);
	pal_bg_bank[2][2]= RGB15(0,31,  0);

    // WHITE
    pal_bg_bank[1][1]= RGB15(15,15,15);
    pal_bg_bank[1][2]= RGB15(31,31,31);
    //pal_bg_bank[0][1]= RGB15(0,  0,  255);
    //pal_bg_bank[0][2]= RGB15(0,  0,  128);
    //pal_bg_bank[0][3]= RGB15(0,  0,  128);


    // (3) Create a map: four contingent blocks of
    //   0x0000, 0x1000, 0x2000, 0x3000.
    SCR_ENTRY *pse= bg0_map;
    //for(ii=0; ii<4; ii++)
        //for(jj=0; jj<32*32; jj++)
    for(int jj=0; jj<nlines*ncols; jj++)
    	*pse++= SE_PALBANK(0) | 0;
    /*bg0_map[0] = SE_PALBANK(0) | 4;
    bg0_map[1] = SE_PALBANK(0) | 5;
    bg0_map[32] = SE_PALBANK(0) | 6;
    bg0_map[33] = SE_PALBANK(0) | 7;*/
    //s16 tile = 11*16;

    // INIT
    for( int z=0; z<ndepth; z++)
        for( int x=0; x<nlines; x++)
        	for( int y=0; y<ncols; y++)
        		map[z][x][y] = EMPTY;

    // SOLID
    for( int z=0; z<6; z++)
        for( int x=0; x<nlines; x++)
        	for( int y=0; y<ncols; y++)
        		map[z][x][y] = WALL0;
    // SURFACE
    for( int x=0; x<nlines; x++)
    	for( int y=0; y<ncols; y++)
    		map[6][x][y] = getGroundTile();

	// CAVE
	for( int x=2; x<13; x++)
		for( int y=5; y<7; y++)
			map[0][x][y] = getGroundTile();

    // WATER
    for( int x=2; x<13; x++)
    	map[6][x][10] = WATER;

    // FOOD
    for( int x=2; x<4; x++)
    	map[6][x][2] = PLANT;

//    setBackgroundTile(tile+0, 0,0);
//    setBackgroundTile(tile+1, 1,0);
//    setBackgroundTile(tile+2, 2,1);
}

void updateBackground( int z){
    for( int x=0; x<nlines; x++)
        for( int y=0; y<ncols; y++)
        	setBackgroundTile( map[z][x][y], x,y, 1,1);
}


// Play a little ditty
const int delay[] = {
		2,2,2,1,1,
		2,1,1,4,
		2,2,2,1,1,
		2,1,1,4,
		2,1,1,2,1,1,
		1,1,1,1,2,1,1};
const int octave[] = {
		1,1,1,1,2,
		1,1,2,1,
		2,2,2,2,2,
		1,1,2,1,
		2,1,1,2,2,2,
		2,2,2,1,2,2,2};
const int notes[] = {
		NOTE_A, NOTE_A, NOTE_A, NOTE_F, NOTE_C,
		NOTE_A, NOTE_F, NOTE_C, NOTE_A,
		NOTE_E, NOTE_E, NOTE_E, NOTE_F, NOTE_C,
		NOTE_GIS, NOTE_F, NOTE_C, NOTE_BES,
		NOTE_A, NOTE_A, NOTE_A, NOTE_GIS, NOTE_G,
		NOTE_FIS, NOTE_F, NOTE_FIS, NOTE_BES, NOTE_DIS, NOTE_D, NOTE_CIS };
int currentNote = -1;
int currentDelay = 0;
void sos()
{
	if(currentDelay==0){
		currentNote++;
		if( currentNote < 9+5+4+6+7){
			currentDelay = delay[ currentNote]*16;
			REG_SND1FREQ = SFREQ_RESET | SND_RATE(notes[currentNote], octave[currentNote]-2);
		}
	}
	currentDelay--;
}

int main(){

	// turn sound on
	    REG_SNDSTAT= SSTAT_ENABLE;
	    // snd1 on left/right ; both full volume
	    REG_SNDDMGCNT = SDMG_BUILD_LR(SDMG_SQR1, 7);
	    // DMG ratio to 100%
	    REG_SNDDSCNT= SDS_DMG100;

	    // no sweep
	    REG_SND1SWEEP= SSW_OFF;
	    // envelope: vol=12, decay, max step time (7) ; 50% duty
	    REG_SND1CNT= SSQR_ENV_BUILD(12, 0, 7) | SSQR_DUTY1_2;
	    REG_SND1FREQ= 0;

	/*
	int ***dist = (int ***)malloc(ndepth*sizeof(int**));
	Position   ***prev = (Position ***)  malloc(ndepth*sizeof(Position**));
	for( int z=0; z<ndepth; z++){
		dist[z] = (int **)malloc(nlines*sizeof(int*));
		prev[z] = (Position **)  malloc(nlines*sizeof(Position*));
		for( int x=0; x<nlines; x++){
			dist[z][x] = (int *)malloc(ncols*sizeof(int));
			prev[z][x] = (Position *)  malloc(ncols*sizeof(Position));
		}
	}
	*/

	//int dist[ndepth][nlines][ncols];
	//Position prev[ndepth][nlines][ncols];


    // SPRITES
	//memcpy32( &pal_obj_mem[0], Phoebus_16x16Pal,   Phoebus_16x16PalLen);
	//memcpy32( &tile_mem[4][0], Phoebus_16x16Tiles, Phoebus_16x16TilesLen/4);
	memcpy32( &pal_obj_mem[0], Jdpage_8x8Pal,   Jdpage_8x8PalLen);
	memcpy32( &tile_mem[4][0], Jdpage_8x8Tiles, Jdpage_8x8TilesLen/4);

	pal_obj_bank[0][15] = RGB15(0,0,0);
	pal_obj_bank[0][1] = RGB15(15,15,0);
	pal_obj_bank[0][2] = RGB15(31,31,0);


	// (2) Initialize all sprites
	oam_init(obj_buffer, 128);

	/*
	// BACKGROUND
    // Load palette
	memcpy32(pal_bg_mem, Phoebus_16x16Pal,   Phoebus_16x16PalLen);
	// Load tiles into CBB 0
	memcpy32(&tile_mem[0][0], Phoebus_16x16Tiles, Phoebus_16x16TilesLen/4/32);
	// Load map into SBB 30
	#define brinMapLen 2048
	//unsigned short brinMap[1024];
	//SCR_ENTRY *pse= brinMap;
	SCR_ENTRY *pse = se_mem[30];
	for(int jj=0; jj<32*32; jj++)
		*pse++= SE_PALBANK(0) | 0;
	//memcpy32(&se_mem[30][0], se_mem[30], brinMapLen/4);



	// set up BG0 for a 4bpp 64x32t map, using
	//   using charblock 0 and screenblock 31
	REG_BG0CNT= BG_CBB(0) | BG_SBB(30) | BG_4BPP | BG_REG_32x32;
	REG_BG0HOFS= 0;
	REG_BG0VOFS= 0;
	//REG_DISPCNT= DCNT_MODE0 | DCNT_BG0;
	 */
	REG_DISPCNT= DCNT_MODE0 | DCNT_BG0 | DCNT_OBJ | DCNT_OBJ_1D;

	//int x= 96, y= 32;
	//u32 tid= 4*'a', pb= 0;      // (3) tile id, pal-bank
	//OBJ_ATTR *metr = &obj_buffer[0];
	//OBJ_ATTR *cursor = &obj_buffer[1];

	init_map();


	Cursor cursor(0);
	List<Dwarf,10> dwarfList;
	dwarfList.add( new Dwarf(1, 4,4,6));
	dwarfList.add( new Dwarf(2, 3,3,6));

	int z = 6;

	int frameCount = 0; // framerate = 60Hz


	while(1)
	{
		vid_vsync();
		key_poll();

		if( frameCount % 60 == 0){
			for(int i=0; i<jobList.size(); i++){
				Job *job = jobList.get(i);
				for(int j=0; j<2 and job->dwarf == 0; j++){
					Dwarf* dwarf = dwarfList[j];
					if( dwarf->job == 0){
						Position ori = dwarf->pos();
						Position dest = job->pos();
						Path* path = 0;
						switch(job->jobtype){
						case DIG:
							path = getPath( &ori, &dest, false, true, 0);break;
						default:
							path = getPath( &ori, &dest, false, false, 0);
						};
						//path = 0;
						if( path){
							job->dwarf = dwarf;

							dwarf->job = job;
							dwarf->path = path;
							dwarf->pathPos = dwarf->path->size()-1;

						}
						//break;
					}
				}

			}
		}


		if(key_is_down(KEY_L) and !key_was_down(KEY_L) and z>0)
			z = z-1;
		if(key_is_down(KEY_R) and !key_was_down(KEY_R) and z<ndepth-1)
			z = z+1;

		for(int j=0; j<2; j++){
			Dwarf* dwarf = dwarfList[j];
			dwarf->update( z);

			char str[20];
			sprintf( str, "Dwarf %d:", j+1);
			putText( str, 17, j*5+1);
			sprintf( str, "H2O:  %3.0f\%", dwarf->waterSupply*100.);
			putText( str, 17, j*5+2);
			sprintf( str, "Food: %3.0f\%", dwarf->foodSupply*100.);
			putText( str, 17, j*5+3);
			putText( "               ", 17, j*5+4);
			if( dwarf->job){
				//const char* blank = ;
				putText( JobNames[dwarf->job->jobtype], 17, j*5+4);
			}

			//sprintf( str, "dwarf %d:", j+1);
			//putText("Build Stairs", 17, 2+j*2);

		}
		cursor.update( z);

		oam_copy(oam_mem, obj_buffer, 3);
		updateBackground( z);

		//setBackgroundTile( 48+z, 0,0, 1,1);
		char str[30];
		sprintf( str, "depth: %d [L/R]", z);
		putText( str, 2, 17);

		sprintf( str, "cmd: %s [A/B]", JobNames[jobType]);
		putText( str, 2, 18);

		/*
		switch(jobType){
		case DIG:
			//putText("Digging", 17, 2);
			setBackgroundTile( 'D', 2,0, 1,1); break;
		case BUILD:
			//putText("Build", 17, 2);
			setBackgroundTile( 'B', 2,0, 1,1); break;
		case B_STAIRS:
			//putText("Build Stairs", 17, 2);
			setBackgroundTile( 'S', 2,0, 1,1); break;

		};*/

		frameCount++;
		sos();

	}
}
