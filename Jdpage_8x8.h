
//{{BLOCK(Jdpage_8x8)

//======================================================================
//
//	Jdpage_8x8, 128x128@4, 
//	Transparent palette entry: 15.
//	+ palette 16 entries, not compressed
//	+ 256 tiles not compressed
//	Total size: 32 + 8192 = 8224
//
//	Time-stamp: 2019-06-24, 20:18:45
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.14
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_JDPAGE_8X8_H
#define GRIT_JDPAGE_8X8_H

#define Jdpage_8x8TilesLen 8192
extern const unsigned int Jdpage_8x8Tiles[2048];

#define Jdpage_8x8PalLen 32
extern const unsigned short Jdpage_8x8Pal[16];

#endif // GRIT_JDPAGE_8X8_H

//}}BLOCK(Jdpage_8x8)
