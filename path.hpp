#ifndef PATH
#define PATH

//typedef signed short    pos_t;
typedef signed char    pos_t;
//typedef unsigned int dist_t;
typedef unsigned short dist_t;

typedef struct{
	pos_t x;
	pos_t y;
	pos_t z;
} Position;
Position operator+(Position &pos1, Position &pos2);
bool operator==(Position &pos1, Position &pos2);

class Path{
	Position *path;
	int length;
	int maxlen;
public:
	Path( int length);
	~Path();
	int size();
	void add( Position pos);
	Position get(int index);
	void print();
};



#endif
