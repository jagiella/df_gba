
//{{BLOCK(Phoebus_16x16)

//======================================================================
//
//	Phoebus_16x16, 256x256@4, 
//	+ palette 16 entries, not compressed
//	+ 1024 tiles Metatiled by 2x2 not compressed
//	Total size: 32 + 32768 = 32800
//
//	Time-stamp: 2019-06-18, 01:43:56
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.14
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_PHOEBUS_16X16_H
#define GRIT_PHOEBUS_16X16_H

#define Phoebus_16x16TilesLen 32768
extern const unsigned int Phoebus_16x16Tiles[8192];

#define Phoebus_16x16PalLen 32
extern const unsigned short Phoebus_16x16Pal[16];

#endif // GRIT_PHOEBUS_16X16_H

//}}BLOCK(Phoebus_16x16)
