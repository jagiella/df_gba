#include <stdlib.h>
#include <stdio.h>

#include "path.hpp"
#include "queue.hpp"

// POSITION

Position operator+(Position &pos1, Position &pos2){
	Position pos = pos1;
	pos.x += pos2.x;
	pos.y += pos2.y;
	pos.z += pos2.z;
	return pos;
};
bool operator==(Position &pos1, Position &pos2){
	return pos1.x == pos2.x and pos1.y == pos2.y and pos1.z == pos2.z;
}


// PATH

Path::Path( int maxlen){
	this->length = 0;
	this->maxlen = maxlen;
	//if( this->maxlen > 10)
	//	this->maxlen = 10;
	path = (Position*) malloc(sizeof(Position)*maxlen);
}
Path::~Path(){
	free(path);
}
int Path::size(){
	return length;
}
void Path::add( Position pos){
	if( length<maxlen){
		path[length] = pos;
		length++;
	}
}
Position Path::get(int index){
	return path[index];
}

void Path::print(){
	for(int i=0; i<length; i++){
		printf("node %d: %d,%d,%d\n", i, path[i].x, path[i].y, path[i].z);
	}
}



