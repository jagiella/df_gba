#ifndef QUEUE
#define QUEUE

#include <stdio.h>
#include <stdlib.h>

template <class T, int N>
class List{
	T *jobList[N];
	int jobCount = 0;
public:
	List(){};
	int size(){return jobCount;}
	T *get(int index){ return jobList[index];}
	bool add(T *job){
		if(jobCount<N){
			jobList[jobCount] = job;
			jobCount++;
			return true;
		}else
			return false;
	}
	void remove(T *job){
		for(int i=0; i<jobCount; i++){
			if( job == jobList[i]){
				jobCount--;
				jobList[i] = jobList[jobCount];
				return;
			}
		}
	}
	T* operator[](int index){
		if( index < jobCount)
			return jobList[index];
		else
			return 0;
	}
};


template <class T, int N>
class PriorityQueue{
public:
	typedef struct element{
		int priority;
		T data;
		struct element* next;
		unsigned short id;
	} QueueElement;
private:
	QueueElement *head;
	QueueElement *stock[N];
	QueueElement  stockElements[N];
	int length = 0;
public:
	PriorityQueue(){
		length = 0;
		head = 0;
		//stock = (QueueElement*) malloc(N*sizeof(QueueElement));
		for(int id=0; id<N; id++)
			stock[id] = &stockElements[id];
	};
	~PriorityQueue(){
		//free(stock);
	};
	QueueElement* newElem(int priority, T data){
		QueueElement *elem = stock[length];
		elem->data = data;
		elem->priority = priority;
		elem->next = 0;
		elem->id = length;
		length++;
		return elem;
	}
	void print(){
		QueueElement* elem = head;
		for(int i=0; i<length; i++){
			printf("[%d/p:%d/i:%d]-",elem, elem->priority, elem->id);
			elem = elem->next;
		}
		printf("next=%d\n", elem);
	}
	void put( int priority, T data){
		//printf("PUT: prio=%d, head=%d\n", priority, head);
		if(length == N)
			return;

		QueueElement* elem = newElem(priority,data);

		QueueElement* start = head;
		if (head == 0 or head->priority >= priority) {
			//printf("NEW HEAD\n");
			// Insert New Node before head
			elem->next = head;
			head = elem;
			//printf("head=%d, head->next=%d\n", head, head->next);
		}
		else {

			// Traverse the list and find a
			// position to insert new node
			while (start->next != 0 &&
				   start->next->priority < priority) {
				start = start->next;
			}

			// Either at the ends of the list
			// or at required position
			elem->next = start->next;
			start->next = elem;
		}

		//print();
	}

	QueueElement get(){
		//printf("GET\n");
		QueueElement *p_current = head;
		int current_id = p_current->id;

		QueueElement temp = *head;
		head = head->next;

		// adapt stock
		length--;

		QueueElement *p_last = stock[length];
		int last_id = p_last->id;

		stock[current_id]     = p_last;
		stock[current_id]->id = current_id;
		stock[last_id]        = p_current;

		return temp;
	}

	int size(){
		return length;
	}

	void clear(){
		length = 0;
		head = 0;
	}

};

/*
template <class T, int N>
class PriorityQueue{
public:

	typedef struct{
		int priority;
		T data;
	} QueueElement;

	QueueElement queue[N];
	int queueLenth;
	//int maxlen = 1000;
	PriorityQueue(){
		queueLenth = 0;
	};
	void clear(){
		queueLenth = 0;
	}
	void put( int priority, T data){
		if(queueLenth<N){
			queue[queueLenth] = {priority, data};
			queueLenth++;
		}
	}
	int size(){
		return queueLenth;
	}
	QueueElement get(){
		int candidateID = 0;
		for( int i=1; i<queueLenth; i++){
			if( queue[i].priority < queue[candidateID].priority)
				candidateID = i;
		}
		QueueElement candidate = queue[candidateID];

		queueLenth--;
		queue[candidateID] = queue[queueLenth];

		return candidate;
	}
};
*/

#endif
