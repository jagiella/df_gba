#include <stdio.h>
#include <stdlib.h>

#include "queue.hpp"
#include "path.hpp"

#define EMPTY ' '
#define WALL0 '#'
#define WATER '~'


#define ndepth 2
#define nlines 16
#define ncols  16

char map[ndepth][nlines][ncols];

/*
template <class T>
typedef struct{
	int dist;
	Position pos;
} QueueElement;
*/

// PATH FINDING

typedef PriorityQueue<Position, 100> PositionQueue;

Path* getPath(Position* ori, Position* dest, bool verbose){

	PositionQueue queue;

    short dist[ndepth][nlines][ncols];
    Position prev[ndepth][nlines][ncols];

    for(int d=0; d<ndepth; d++)
        for(int l=0; l<nlines; l++)
        	for(int c=0; c<ncols; c++){
				dist[d][l][c] = 1000;
				prev[d][l][c] = {0,0,0};
			}

    queue.put( 0, *ori);
    dist[ ori->z][ ori->x][ori->y] = 0;
    prev[ ori->z][ ori->x][ori->y] = *ori;

    // neighbor transitions
    //Position neighborHood[4] = {{-1,0}, {+1,0}, {0,-1},{0,+1}};
    Position neighborHood[6] = {
    		{-1, 0, 0},
			{+1, 0, 0},
			{ 0,-1, 0},
			{ 0,+1, 0},
			{ 0, 0,-1},
			{ 0, 0,+1}
    };

    while( queue.size() > 0){
    	if( verbose)
    		printf("QUEUE LENGTH: %d\n", queue.size());
    	PositionQueue::QueueElement entry = queue.get();
    	Position pos = entry.data;
    	int cost = entry.priority;
    	//printf("current node: %d, %d, %d -> cost: %d\n", pos.x, pos.y, pos.z, cost);

    	for( int i=0; i<6; i++){
    		Position neighbor;
    		neighbor = pos + neighborHood[i];
    		if( neighbor.x >= 0 and neighbor.y>=0 and neighbor.z>=0 and neighbor.x < nlines and neighbor.y < ncols and neighbor.z < ndepth){
    			if( verbose)
    			    printf("neighbor node: %d, %d, %d\n", neighbor.x, neighbor.y, neighbor.z);
				//printf("proceed!\n");
    			if( neighbor == *dest){
    				//printf( "found!\n");

    				Path *path = new Path( dist[ pos.z][ pos.x][ pos.y]);
    				printf("build path of length: %d\n", dist[ pos.z][ pos.x][ pos.y]);
					//if( include_dest):
					//	path.append( dest)

					Position *current = &pos;
					while( !(*current == *ori)){
						path->add( *current);
						current = &prev[ current->z][ current->x][ current->y];
						//#print current
					}

					return path;

    				//return 0;
    			}else{
    				if( map[ neighbor.z][ neighbor.x][ neighbor.y] == EMPTY){
						int new_cost = dist[ pos.z][ pos.x][ pos.y] + 1;
						if( dist[neighbor.z][neighbor.x][ neighbor.y] > new_cost){
							dist[neighbor.z][neighbor.x][ neighbor.y] = new_cost;
							prev[neighbor.z][neighbor.x][ neighbor.y] = pos;
							if( verbose)
							    printf("put neighbor in queue\n");
							queue.put( dist[neighbor.z][neighbor.x][ neighbor.y], neighbor);
						}
    				}
    			}
    		}


    	}

    }

    if( verbose)
    	printf( "Tile not reachable!\n");
    return 0;
}



int main(){
    for( int z=0; z<ndepth; z++)
        for( int x=0; x<nlines; x++)
        	for( int y=0; y<ncols; y++)
        		map[z][x][y] = EMPTY;

    // CAVE
	for( int x=0; x<15; x++)
		for( int y=5; y<7; y++)
			map[0][x][y] = WALL0;

    // WATER
    for( int x=2; x<13; x++)
    	map[0][x][10] = WATER;


	Position ori = {4,4,0};
	Position dest = {10,10,0};
	Path* path = getPath( &ori, &dest, true);
	path->print();
	/*

	for(int i=0; i<path->size(); i++){
				//printf("node %d: %d,%d\n", i, path[i].x, path[i].y);
		Position pos = path->get(i);
		if( pos.z==0)
			map[0][pos.x][pos.y] = 'o';
	}

    for( int x=0; x<nlines; x++){
        for( int y=0; y<ncols; y++)
        	printf( "%c", map[0][y][x]);
        printf("\n");
    }
	*/

	delete path;

	return 0;
}
